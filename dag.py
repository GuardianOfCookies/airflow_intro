from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.postgres_operator import PostgresOperator
from config import CONFIG
import datetime as dt
import requests as r
import json

default_args = {
    'owner': 'airflow',
    'depends_on_past': True,
    'start_date':  dt.datetime(2020, 6, 5),
    'retries': 3,
    'retry_delay': dt.timedelta(minutes=5)}

dag = DAG(
    'transport_api',
    default_args=default_args,
    description='Yandex api Pipeline',
    schedule_interval=dt.timedelta(days=1)
)

def extract_operator(**context):
    def random_date():
        from datetime import date 
        import random 
        start_dt = date.today().toordinal() 
        end_dt = date.today().replace(month=9).toordinal() 
        random_day = date.fromordinal(random.randint(start_dt, end_dt)) 
        return str(random_day)


    def request(date:str, event: str, offset: int):
        url = f'https://api.rasp.yandex.net/v3.0/schedule/?apikey={CONFIG["yandex_api_key"]}&station=TJM&transport_types=plane&date={date}&event={event}&system=iata&offset={offset}'
        result=json.loads(r.get(url).text)
        if 'error' in result:
            raise Exception(result['error']['text'])
        return result

    date = random_date()
    total_flights = []
    for event in ['arrival','departure']:
        offset = 0
        response = request(date , event, offset)
        recieved = response['schedule']
        total = response['pagination']['total']
        while len(recieved) <  total:
            offset = offset + 100
            recieved = recieved + request(date ,event, offset)['schedule']
        total_flights = total_flights + recieved
        
    return {'date': date, 'flights': total_flights}



def transform_operator(**context):
    def wrap(val):
        if val!=None:
            return "'" + val + "'"
        return 'null'

    loaded_data = context['ti'].xcom_pull(key=None, task_ids='extract')
    cleared_flights_info=[]
    for flight in loaded_data['flights']:
        cleared_flights_info.append(dict(
                            departure=wrap(flight['departure']), 
                            arrival=wrap(flight['departure']), 
                            title=wrap(flight['thread']['title']),
                            date=wrap(loaded_data['date']),
                            event=wrap('arrival' if  flight['arrival'] else 'departure'),
                            is_fuzzy=flight['is_fuzzy'],
                            uid=wrap(flight['thread']['uid']),
                            number=wrap(flight['thread']['number']),
                            vehicle=wrap(flight['thread']['vehicle']),
                            carrier=wrap(flight['thread']['carrier']['title'])
        ))
    return cleared_flights_info

load_query='''{% set flights = ti.xcom_pull(task_ids='transform') %}
            {% for flight in flights %}
SELECT * from insert_flight({{ flight.date }} ::date, {{ flight.event }}::text, {{ flight.arrival }}::timestamp, {{ flight.departure }}::timestamp, {{ flight.title }}::text, {{ flight.is_fuzzy }}::boolean, {{ flight.uid }}::text, {{ flight.number }}::text,
 {{ flight.vehicle }}::text, {{ flight.carrier }}::text);
{% endfor %}
'''

with dag:
    extract = PythonOperator(
        default_args=default_args,
        task_id='extract',
        python_callable=extract_operator,
        provide_context=True)


    transform = PythonOperator(
            default_args=default_args,
            task_id='transform',
            python_callable=transform_operator,
            provide_context=True)

    load = PostgresOperator(
            task_id='load',
            sql=load_query,
            postgres_conn_id='flights',
            autocommit=True,
            provide_context=True,
            database="airport",
            dag=dag)

extract >> transform >> load