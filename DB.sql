CREATE TABLE "Flight" (
	"id" serial NOT NULL,
	"date" date,
	"type" character varying(10) NOT NULL,
	"arrival_time" TIMESTAMP,
	"departure_time" TIMESTAMP,
	"is_fuzzy" BOOLEAN NOT NULL,
	"unit_id" serial NOT NULL,
	CONSTRAINT "Flight_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Unit" (
	"id" serial NOT NULL,
	"uid" character varying(100) NOT NULL,
	"title" character varying(50) NOT NULL,
	"number" character varying(15) NOT NULL,
	"vehicle" character varying(30) NOT NULL,
	"carrier" character varying(30) NOT NULL,
	CONSTRAINT "Unit_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



ALTER TABLE "Flight" ADD CONSTRAINT "Flight_fk0" FOREIGN KEY ("unit_id") REFERENCES "Unit"("id");


