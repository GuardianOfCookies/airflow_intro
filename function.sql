﻿CREATE OR REPLACE FUNCTION insert_flight(date_v date, type_v character varying, arrival_time_v timestamp, departure_time_v timestamp, title_v  character varying, is_fuzzy_v boolean, 
				 uid_v character varying, number_v character varying, vehicle_v character varying, carrier_v character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
  idx int;
BEGIN
	SELECT id FROM "Unit" WHERE uid=uid_v into idx;
	IF idx is NULL  THEN
		INSERT INTO "Unit" (uid, title, number, vehicle, carrier) values(uid_v, title_v, number_v, vehicle_v, carrier_v) RETURNING id INTO idx;
	END IF;
	INSERT INTO "Flight"(date, type, arrival_time, departure_time, is_fuzzy, unit_id) VALUES(date_v, type_v, arrival_time_v, departure_time_v, is_fuzzy_v, idx);
	return ;
	
END
$$;

--SELECT * from insert_flight('2020-06-10' ::date,'arrival', '2020-06-11T01:05:00+05:00'::timestamp, null::timestamp, 'Геленджик — Тюмень'::text, 0::boolean, 'UT-592_0_c29_548'::text, 'UT 590'::text, 'Boeing 737-500'::text, 'S8'::text);

